// class Ligne{
//     constructor(nom,longueur){
//         this.nom=nom;
//         this.longueur=longueur;
//     }
//     taille(){
//         console.log(`Longueur de 5`+this.nom+`:`+this.longueur)
//     };
// }
// let geo1=new Ligne('geo1',10);
// let geo2=new Ligne('geo2',5);
// geo1.taille();
// geo2.taille();
function Ligne(nom,longueur){
    this.nom=nom;
    this.longueur=longueur;
}
Ligne.prototype.taille=function(){
    console.log(`Longueur de 5`+this.nom+`:`+this.longueur);
}
let geo1=new Ligne('geo1',10);
let geo2=new Ligne('geo2',5);
geo1.taille();
geo2.taille();